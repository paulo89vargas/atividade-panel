﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Inicial.aspx.cs" Inherits="AtividadePanel.Inicial" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Atividade Panel</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    <form runat="server">
        <div class="d-flex flex-column justify-content-center">
            <div class="d-flex justify-content-center mt-5">
                <h1>Exemplo do controle Panel em ASP.NET C# </h1>
            </div>
            <div class="d-flex justify-content-center mt-3">
                <div class="mt-3">
                    <asp:Panel ID="Panel1" runat="server" class="justify-content-center">
                        <asp:Panel ID="Panel2" runat="server">
                            <div class="justify-content-center">
                                <div>
                                    <strong>
                                        <span class="d-flex justify-content-center mt-3">Informações Pessoais
                                        </span>
                                    </strong>
                                </div>
                                <div class="d-flex flex-column justify-content-center">
                                    <div class="my-3">
                                        Nome
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxNome" runat="server"></asp:TextBox>
                                    </div>
                                    <div>
                                        <asp:Label class="form-label" ID="ErrorNome" ForeColor="Red" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="my-3">
                                        Sobrenome
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxSobrenome" runat="server"></asp:TextBox>
                                    </div>
                                    <div>
                                        <asp:Label class="form-label" ID="ErrorSobrenome" ForeColor="Red" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div>
                                    <div class="my-3">
                                        Gênero
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxGenero" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div>
                                    <div class="my-3">
                                        Celular
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxCelular" runat="server"></asp:TextBox>
                                    </div>
                                    <div>
                                        <asp:Label class="form-label" ID="ErrorCelular" ForeColor="Red" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div>
                                    <div>
                                        <div class="my-3">
                                            <asp:Button class="btn btn-success mx-3" ID="btnNextPanel2" runat="server" OnClick="btnNextPanel2_Click" Text="Próximo" />
                                        </div>
                                    </div>
                                </div>
                                <asp:GridView ID="gridView2" runat="server" AutoGenerateColumns="true" Visible="true">
                                </asp:GridView>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="Panel3" runat="server" Visible="false">
                            <div class="justify-content-center">
                                <div>
                                    <strong>
                                        <span class="d-flex justify-content-center mt-3">Detalhes do Endereço
                                        </span>
                                    </strong>
                                </div>
                                <div class="d-flex flex-column justify-content-center">
                                    <div class="my-3">
                                        Endereço
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxEndereco" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="my-3">
                                        Cidade
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxCidade" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="my-3">
                                        Cep
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxCep" runat="server"></asp:TextBox>
                                    </div>
                                    <div>
                                        <asp:Label class="form-label" ID="ErrorCep" ForeColor="Red" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                                <div>
                                </div>
                                <div class="d-flex flex-row">
                                    <div class="my-3">
                                        <asp:Button class="btn btn-dark mx-3" ID="btnBackPanel3" runat="server" Text="Voltar" OnClick="btnBackPanel3_Click" />
                                    </div>
                                    <div class="my-3">
                                        <asp:Button class="btn btn-success" ID="btnNextPanel3" runat="server" Text="Próximo" OnClick="btnNextPanel3_Click" />
                                    </div>
                                </div>
                                <div>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="Panel4" runat="server" Visible="false">
                            <div class="justify-content-center">
                                <div>
                                    <div>
                                        <strong>
                                            <span class="d-flex justify-content-center mt-3">Área de Login
                                            </span>
                                        </strong>
                                    </div>
                                </div>
                                <div class="d-flex flex-column justify-content-center">
                                    <div class="my-3">
                                        Usuário
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxUsuario" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="my-3">
                                        Senha
                                    </div>
                                    <div>
                                        <asp:TextBox class="form-control" ID="TextBoxSenha" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-row">
                                <div class="my-3">
                                    <asp:Button class="btn btn-dark mx-3" ID="btnBackPanel4" runat="server" Text="Voltar" OnClick="btnBackPanel4_Click" />
                                </div>
                                <div class="my-3">
                                    <asp:Button class="btn btn-success" ID="btnSendPanel4" runat="server" Text="Enviar" OnClick="btnSendPanel4_Click" />
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="Panel5" runat="server" Visible="false">
                            <div>
                                <div class="d-flex flex-row">
                                    <h2 class="mb-5">Seus dados foram enviados com sucesso !
                                    </h2>
                                </div>
                                <div class="d-flex justify-content-center bor">
                                    <asp:GridView ID="gridView1" runat="server" ShowHeader="false" class="d-flex flex-row">
                                        <HeaderStyle BackColor="LightGray" />
                                        <AlternatingRowStyle BackColor="LightCyan" />
                                    </asp:GridView>
                                </div>

                                <div class="d-flex flex-row justify-content-center">
                                    <div class="my-3">
                                        <asp:Button class="btn btn-dark mx-3" ID="Button1" runat="server" Text="Voltar" OnClick="btnBackPanel4_Click" />
                                    </div>
                                    <div class="my-3">
                                        <asp:Button class="btn btn-success" ID="Button2" runat="server" Text="Sair" OnClick="Button2_Click" />
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
