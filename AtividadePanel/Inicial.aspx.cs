﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AtividadePanel
{
    public partial class Inicial : System.Web.UI.Page
    {
        Dictionary<string, string> lstDados = new Dictionary<string, string>();

        protected void Page_Load(object sender, EventArgs e)
        {
           
            gridView1.DataSource = lstDados;
            gridView1.DataBind();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void btnNextPanel2_Click(object sender, EventArgs e)
        {
            
            string padraoCelu = @"^\(?\d{2}\)?[-.\s]?\d{4,5}[-.\s]?\d{4}$";

            Regex regexCelu = new Regex(padraoCelu);

            if (!regexCelu.IsMatch(TextBoxCelular.Text))
            {
                ErrorCelular.Text = "Número de celular inválido.";
                return;
            }

            string padraoNome = "^[A-Za-z]+$";
            Regex regexNome = new Regex(padraoNome);

            if (!regexNome.IsMatch(TextBoxNome.Text))
            {
                ErrorNome.Text = "O campo deve conter somente letras !";
                return;
            }

            if (!regexNome.IsMatch(TextBoxSobrenome.Text))
            {
                ErrorSobrenome.Text = "O campo deve conter somente letras !";
                return;
            }

            ErrorCelular.Text = "";
            ErrorNome.Text = "";
            ErrorSobrenome.Text = "";

            Panel2.Visible = false;
            Panel3.Visible = true;
            lstDados["Nome"] = TextBoxNome.Text;
            lstDados["Sobrenome"] = TextBoxSobrenome.Text;
            lstDados["Genero"] = TextBoxGenero.Text;
            lstDados["Celular"] = TextBoxCelular.Text;
        }

        protected void btnBackPanel3_Click(object sender, EventArgs e)
        {
            Panel2.Visible = true;
            Panel3.Visible = false;
        }

        protected void btnNextPanel3_Click(object sender, EventArgs e)
        {
            string padraoCep = @"^\d{5}-\d{3}$";

            Regex regexCep = new Regex(padraoCep);

            if (!regexCep.IsMatch(TextBoxCep.Text))
            {
                ErrorCep.Text = "Cep inválido.";
                return;
            }

            ErrorCep.Text = "";
            Panel3.Visible = false;
            Panel4.Visible = true;
            lstDados["Endereço"] = TextBoxEndereco.Text;
            lstDados["Cidade"] = TextBoxCidade.Text;
            lstDados["Cep"] = TextBoxCep.Text;
        }

        protected void btnBackPanel4_Click(object sender, EventArgs e)
        {
            Panel3.Visible = true;
            Panel4.Visible = false;
            Panel5.Visible = false;
            lstDados["Usuario"] = TextBoxUsuario.Text;
            lstDados["Senha"] = TextBoxSenha.Text;
        }

        protected void btnSendPanel4_Click(object sender, EventArgs e)
        {
            Panel4.Visible = false;
            Panel5.Visible = true;
            lstDados["Nome :"] = TextBoxNome.Text;
            lstDados["Sobrenome :"] = TextBoxSobrenome.Text;
            lstDados["Genero :"] = TextBoxGenero.Text;
            lstDados["Celular :"] = TextBoxCelular.Text;
            lstDados["Endereço :"] = TextBoxEndereco.Text;
            lstDados["Cidade :"] = TextBoxCidade.Text;
            lstDados["Cep :"] = TextBoxCep.Text;
            lstDados["Usuario :"] = TextBoxUsuario.Text;
            lstDados["Senha :"] = TextBoxSenha.Text;
            gridView1.DataSource = lstDados;
            gridView1.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "FecharJanela", "window.close();", true);
        }
    }
}